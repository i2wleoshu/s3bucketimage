//
//  ViewController.swift
//  S3BuketImageExample
//
//  Created by Leo_hsu on 2018/9/16.
//  Copyright © 2018年 Leo. All rights reserved.
//

import UIKit
import S3BucketImage

let IMG_NAME = "files_1536723205546.png"

class ViewController: UIViewController {

    @IBOutlet weak var imageview: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let aws = S3BucketImp(imgName: IMG_NAME)
        imageview.s3_setImage(aws: aws, options: .lowPriority)
    }

}

