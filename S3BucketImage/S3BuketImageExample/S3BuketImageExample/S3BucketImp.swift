//
//  S3BucketImp.swift
//  S3BuketImageExample
//
//  Created by Leo_hsu on 2018/9/16.
//  Copyright © 2018年 Leo. All rights reserved.
//

import Foundation
import S3BucketImage
import Arcane

let BUCKET_NAME: String = "Your bucket name"
let ACCESS_KEY_ID: String = "Your access key ID"
let SECRET_ACCESS_KEY: String = "Your secret access key"

struct S3BucketImp: AWS_S3Bucket {
    var secretKey = SECRET_ACCESS_KEY
    var accessKey = ACCESS_KEY_ID
    var bucket = BUCKET_NAME
    var host: String {
        return "\(bucket).s3.amazonaws.com"
    }
    var currentDate: String {
        return current
    }
    
    private var current: String!
    private var imgName: String!
    
    init(imgName: String) {
        self.current = getCurrentDate()
        self.imgName = imgName
    }
    
    func generateGetOAuthAWS() -> String {
        let resource = "/" + bucket + "/" + imgName
        print("resource = \(resource)")
        let formattedDate: String = currentDate
        let stringToSign = "GET" + "\n\n\n" + formattedDate + "\n" + resource
        let utf8SecretKey = secretKey.data(using: .utf8)!
        let utf8StringToSign = stringToSign.data(using: .utf8)!
        let hmacSHA1: Data = HMAC.SHA1(utf8StringToSign, key: utf8SecretKey)!
        var signature = hmacSHA1.base64String
        signature = signature.replacingOccurrences(of: "\n", with: "")
        return  "AWS " + accessKey + ":" + signature
    }
    
    func generateS3ImageUrl() -> URL {
        return URL(string: "https://\(bucket).s3.amazonaws.com/\(imgName!)")!
    }
    
    private func getCurrentDate() -> String {
        let todaysDate:Date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE',' dd MMM yyyy HH':'mm':'ss z"
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        let dateInFormat: String = dateFormatter.string(from: todaysDate)
        print("date = \(dateInFormat)")
        return dateInFormat
    }
}
