//
//  File.swift
//  S3BucketImage
//
//  Created by Leo_hsu on 2018/9/16.
//  Copyright © 2018年 Leo. All rights reserved.
//

import UIKit
import SDWebImage

extension UIImageView {
    
    /// Signing and Authenticating REST Requests from Amazon s3 bucket. And download image by SDWebImage.
    ///
    /// - Parameters:
    ///   - aws: A AWS_S3Bucket concrete. That should have secretKey, accessKey, bucket.
    ///   - placeholderImage: The image to be set initially, until the image request finishes.
    ///   - options: The options to use when downloading the image. @see SDWebImageOptions for the possible values.
    ///   - progress: A block called while image is downloading
    ///                       @note the progress block is executed on a background queue
    ///   - completed: It will download image in background, then set image to imageView in main thread.
    public func s3_setImage(aws: AWS_S3Bucket, placeholderImage: UIImage? = nil, options: SDWebImageOptions, progress: SDWebImageDownloaderProgressBlock? = nil, completed: SDExternalCompletionBlock? = nil) {
        let oauth = aws.generateGetOAuthAWS()
        let url = aws.generateS3ImageUrl()
        let loader = SDWebImageManager.shared().imageDownloader!
        loader.setValue("\(aws.host)" , forHTTPHeaderField: "Host")
        loader.setValue("\(aws.currentDate)" , forHTTPHeaderField: "Date")
        loader.setValue("\(oauth)", forHTTPHeaderField: "Authorization")
        self.sd_setImage(with: url, placeholderImage: placeholderImage, options: options, progress: progress, completed: completed)
    }
}
