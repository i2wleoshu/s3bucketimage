//
//  AWS_S3Bucket.swift
//  S3BucketImage
//
//  Created by Leo_hsu on 2018/9/16.
//  Copyright © 2018年 Leo. All rights reserved.
//

import Foundation

public protocol AWS_S3Bucket {
    var secretKey: String {get}
    var accessKey: String {get}
    var bucket: String{get}
    var host: String{get}
    var currentDate: String{get}
    func generateGetOAuthAWS() -> String
    func generateS3ImageUrl() -> URL
}
