# S3BucketImage

Base on [SDWebImage](https://github.com/rs/SDWebImage), S3BucketImage could help to download Amazon s3 bucket image. Signing and Authenticating REST Requests from Amazon s3 bucket. Then download image by SDWebImage.

## How to use

###### ViewController
```swift
import S3BucketImage

let aws = S3BucketImp(imgName: IMG_NAME)
imageview.s3_setImage(aws: aws, options: .lowPriority)
```


###### Implement AWS_S3Bucket Protocol
```swift
import S3BucketImage

struct S3BucketImp: AWS_S3Bucket {
    var secretKey = "SECRET_ACCESS_KEY"
    var accessKey = "ACCESS_KEY_ID"
    var bucket = "BUCKET_NAME"
    var host: String {
        return "\(bucket).s3.amazonaws.com"
    }
    var currentDate: String {
        return current
    }

    private var current: String!
    private var imgName: String!

    init(imgName: String) {
        self.current = getCurrentDate()
        self.imgName = imgName
    }

    func generateGetOAuthAWS() -> String {
        let resource = "/" + bucket + "/" + imgName
        print("resource = \(resource)")
        let formattedDate: String = currentDate
        let stringToSign = "GET" + "\n\n\n" + formattedDate + "\n" + resource
        let utf8SecretKey = secretKey.data(using: .utf8)!
        let utf8StringToSign = stringToSign.data(using: .utf8)!
        let hmacSHA1: Data = HMAC.SHA1(utf8StringToSign, key: utf8SecretKey)!
        var signature = hmacSHA1.base64String
        signature = signature.replacingOccurrences(of: "\n", with: "")
        return  "AWS " + accessKey + ":" + signature
    }

    func generateS3ImageUrl() -> URL {
        return URL(string: "https://\(bucket).s3.amazonaws.com/\(imgName!)")!
    }

    private func getCurrentDate() -> String {
        let todaysDate:Date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE',' dd MMM yyyy HH':'mm':'ss z"
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        let dateInFormat: String = dateFormatter.string(from: todaysDate)
        print("date = \(dateInFormat)")
        return dateInFormat
    }
}
```

---
## Installation with CocoaPods
#### Podfile
    platform :ios, '10.0'
    use_frameworks!
    pod 'S3BucketImage'


---

##### Author

| Name | Email | Position |
| ------------- |:-------------:| -----:|
| Leo Hsu | [i2wleoshu@gmail.com](mailto://i2wleoshu@gmail.com) |   member |



---

##### License
All source code is licensed under the [MIT License](https://gitlab.com/i2wleoshu/s3bucketimage/blob/master/LICENSE).
